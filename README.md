# Kjeeves

## Installation

### Using docker-compose

```sh
docker-compose build
docker-compose up -d
docker-compose exec kjeeves_backend python manage.py migrate
```
